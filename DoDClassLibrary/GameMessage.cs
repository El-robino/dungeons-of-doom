namespace DoDClassLibrary
{
    public class GameMessage
    {
        public string Message { get; private set; }
        public Status Status { get; private set; }
        public GameMessage(string message, Status status)
        {
            Message = message;
            Status = status;
        }

        public override string ToString()
        {
            return Message;
        }
    }
}