﻿using DoDClassLibrary.Game_Objects.Character_Classes;
using DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes;
using DoDClassLibrary.Game_Objects.Item_Classes;
using DoDClassLibrary.World_Classes;

namespace DoDClassLibrary
{
    public class Game
    {
        private readonly IGamePresenter _gamePresenter;

        public Game(IGamePresenter gamePresenter)
        {
            _gamePresenter = gamePresenter;
            Player = CreatePlayer();
            World = new World(10, 10, Player);
        }
        //TODO only class that may interact with the user
        private Player Player { get; }
        private World World { get; set; }

        public void Start()
        {
            do
            {
                _gamePresenter.NewFrame();
                _gamePresenter.DisplayPlayerInfo(Player);
                _gamePresenter.DisplayWorld(World);
                _gamePresenter.PrintFromQueue();
                _gamePresenter.UserControls();
                Events();
                if (Enemy.Ammount == 0)
                {
                    _gamePresenter.DisplayMessage("You Defeated every monster! press any key to get a new world");
                    _gamePresenter.ReadKeyInput();
                    World = new World(10, 10, Player);
                }
            } while (!Player.Dead);
        }
        private void Events()
        {
            switch (_gamePresenter.Control)
            {
                case Control.MoveUp:
                    World.Move(Player, 0, -1);
                    break;
                case Control.MoveLeft:
                    World.Move(Player, -1, 0);
                    break;
                case Control.MoveDown:
                    World.Move(Player, 0, 1);
                    break;
                case Control.MoveRight:
                    World.Move(Player, 1, 0);
                    break;
            }
            var currentSquare = Player.CurrentSquare;
            var item = currentSquare.GetPawn<Equipment>();
            if (item != null)
            {
                _gamePresenter.PrintQueue.Enqueue(new GameMessage($"You're standing on {item} press e to pick it up", Status.Positive));
                if (_gamePresenter.Control == Control.Use)
                {
                    Player.Inventory.Add(item);
                    currentSquare.Pawns.Remove(item);
                    _gamePresenter.PrintQueue.Enqueue(new GameMessage($"you Looted {Player.Inventory[Player.Inventory.Count - 1]}", Status.Positive));
                }
            }
            var enemy = currentSquare.GetPawn<Enemy>();
            if (enemy != null)
            {
                _gamePresenter.PrintQueue.Enqueue(new GameMessage($"Press e to fight {enemy}", Status.Negative));
                if (_gamePresenter.Control == Control.Use)
                {
                    while (!enemy.Dead)
                    {
                        _gamePresenter.NewFrame();
                        Player.Fight(enemy, OnAttack);
                        _gamePresenter.ReadKeyInput();
                    }
                    for (var i = 0; i < 5; i++)
                    {
                        switch (_gamePresenter.PickStat(Player))
                        {
                            case StatMenu.Agility:
                                Player.BaseStats = Player.BaseStats.Add(1, 0, 0);
                                break;
                            case StatMenu.Luck:
                                Player.BaseStats = Player.BaseStats.Add(0, 1, 0);
                                break;
                            case StatMenu.Strength:
                                Player.BaseStats = Player.BaseStats.Add(0, 0, 1);
                                break;
                        }
                    }
                    _gamePresenter.PrintQueue.Enqueue(new GameMessage(Player.LevelUp(), Status.Positive));
                    currentSquare.Pawns.Remove(enemy);
                    Enemy.Ammount--;
                }
            }
            if (_gamePresenter.Control != Control.Inventory) return;
            var choosenItem = _gamePresenter.InventoryMenu(Player);
            while (choosenItem != null)
            {
                Player.Equip(Player.Inventory[choosenItem.Value]);
                choosenItem = _gamePresenter.InventoryMenu(Player);
            }
        }
        private void OnAttack(string message) => _gamePresenter.DisplayMessage(message);
        private Player CreatePlayer()
        {
            _gamePresenter.DisplayMessage("Spelarens namn");
            string name;
            do
            {
                name = _gamePresenter.InputText();
                if (name.Length <= 2)
                    _gamePresenter.DisplayMessage("skriv minst tre tecken", Status.Negative);
                else
                    break;
            } while (true);
            var player = new Player(name, "p");
            return player;
        }
    }
}
