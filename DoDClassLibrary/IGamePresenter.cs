using DoDClassLibrary.Game_Objects.Character_Classes;
using DoDClassLibrary.World_Classes;
using System.Collections.Generic;

namespace DoDClassLibrary
{
    public interface IGamePresenter
    {
        Queue<GameMessage> PrintQueue { get; }
        void DisplayWorld(World world);
        void DisplayPlayerInfo(Player player);
        void DisplayMessage(string header, Status status = Status.Default);
        void UserControls();
        void PrintFromQueue();
        Control Control { get; }
        StatMenu PickStat(Character character);
        int? InventoryMenu(Player player);
        void NewFrame();
        void ReadKeyInput();
        string InputText();
    }


}