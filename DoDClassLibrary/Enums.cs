﻿namespace DoDClassLibrary
{
    public enum Status
    {
        Positive,
        Negative,
        Misc,
        Default
    }

    public enum Control
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Use,
        Inventory,
        Escape
    }

    public enum StatMenu
    {
        Strength,
        Luck,
        Agility
    }
}
