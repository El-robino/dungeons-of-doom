﻿using System;
using System.Collections.Generic;
using DoDClassLibrary.Game_Objects;
using DoDClassLibrary.Game_Objects.Character_Classes;
using DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes.Type;
using DoDClassLibrary.Game_Objects.Item_Classes.type;
using DoDClassLibrary.Utilities;

namespace DoDClassLibrary.World_Classes
{
    public class World
    {
        public readonly int Height;
        public readonly Square[,] Squares;
        public readonly int Width;
        private static int _level;
        public World(int width, int height, Pawn player)
        {
            Width = width;
            Height = height;
            Squares = new Square[Width, Height];
            CreateGrid(player);
            _level++;
        }

        private void CreateGrid(Pawn player)
        {
            var spawnChance = new Chance(50);
            var itemSpawnChance = new Chance(25);
            var monsterSpawnChance = new Chance(25);
            for (var y = 0; y < Height; y++)
            {
                for (var x = 0; x < Width; x++)
                {
                    if (spawnChance.Evaluate(x, y))
                    {
                        if (spawnChance.Evaluate())
                        {
                            if (itemSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Weapon(Generate.Name("Sword"), "W", damage: 5) });
                            else if (itemSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Weapon(Generate.Name("Mace"), "W", damage: 5) });
                            else if (itemSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Armor(Generate.Name("Armor"), "A", defense: 20) });
                            else
                                Squares[x, y] = new Square(new List<Pawn> { new Weapon(Generate.Name("Axe"), "W", damage: 5) });
                        }
                        else
                        {
                            if (monsterSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Ogre("Ogre", "e") });
                            else if (monsterSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Orc("Orc", "e") });
                            else if (monsterSpawnChance.Evaluate())
                                Squares[x, y] = new Square(new List<Pawn> { new Demon("Demon", "e") });
                            else
                                Squares[x, y] = new Square(new List<Pawn> { new Troll("Troll", "e") });
                        }
                    }
                    else if (x == 0 && y == 0)
                    {
                        Squares[x, y] = new Square(new List<Pawn> { player });
                    }
                    else
                        Squares[x, y] = new Square(new List<Pawn>());
                }
            }
        }
        public void Move(Character character, int dx, int dy)
        {
            for (var y = 0; y < Height; y++)
            {
                for (var x = 0; x < Width; x++)
                {
                    if (Squares[x, y].Pawns.Contains(character))
                    {
                        Squares[x, y].Pawns.Remove(character);
                        var position = Squares[Mod(x + dx, Width), Mod(y + dy, Height)];
                        character.CurrentSquare = position;
                        position.Pawns.Add(character);
                        return;
                    }
                }
            }
        }



        //Correct Working Modolus
        private static int Mod(int a, int n)
        {
            return a - (int)Math.Floor((double)a / n) * n;
        }
    }
}