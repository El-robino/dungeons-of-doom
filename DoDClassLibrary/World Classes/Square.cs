﻿using System.Collections.Generic;
using System.Linq;
using DoDClassLibrary.Game_Objects;

namespace DoDClassLibrary.World_Classes
{
    public struct Square
    {
        public readonly List<Pawn> Pawns;

        public Square(List<Pawn> pawns)
        {
            Pawns = pawns;
        }

        public T GetPawn<T>()
        {
            //Either null or subtype of pawn
            return Pawns.OfType<T>().FirstOrDefault();
        }

        //public override string ToString()
        //{
        //    var pawn = Pawns.OrderByDescending(o => o.DisplayPriority).FirstOrDefault();
        //    return pawn == null ? "[ ]" : $"[{pawn.Icon}]";
        //}
    }
}