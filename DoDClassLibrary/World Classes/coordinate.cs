﻿using System;
using System.Collections.Generic;
using Dungeons_Of_Doom.Game_Objects;

namespace Dungeons_Of_Doom.World_Classes
{
    internal struct Coordinate
    {
        public readonly int X;
        public readonly int Y;

        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Coordinate North(int wordlHeight)
        {
            return new Coordinate(X, Y > 0 ? Y - 1 : wordlHeight - 1);
        }

        public Coordinate West(int worldWidth) => new Coordinate(X > 0 ? X - 1 : worldWidth - 1, Y);
        public Coordinate East(int worldWidth) => new Coordinate(X < worldWidth - 1 ? X + 1 : 0, Y);
        public Coordinate South(int worldHeight) => new Coordinate(X, Y < worldHeight - 1 ? Y + 1 : 0);
    }
}