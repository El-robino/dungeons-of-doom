﻿using System;

namespace DoDClassLibrary.Game_Objects.Item_Classes
{
    public abstract class Item : Pawn
    {
        protected static readonly Random Random = new Random();

        protected Item(string name, string icon) : base(name, icon)
        {
            DisplayPriority = 1;
            Icon = icon;
            Name = name;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}