﻿using DoDClassLibrary.Game_Objects.Character_Classes;

namespace DoDClassLibrary.Game_Objects.Item_Classes
{
    internal abstract class Equipment : Item
    {
        public Stats Stats;
        public int Damage { get; }
        private int Defense { get; }

        protected Equipment(string name, string icon, int defense = 0, int damage = 0) : base(name, icon)
        {
            Defense = defense;
            Damage = damage;
            Stats.Strength = Random.Next(5);
            Stats.Luck = Random.Next(5);
            Stats.Agility = Random.Next(5);
        }
        public override string ToString()
        {
            return $"{Name} Dmg: {Damage / 4} - {Damage / 2} Defense {Defense} stats: Strength:{Stats.Strength}, Agility: {Stats.Agility}, Luck {Stats.Luck}";
        }
    }
}