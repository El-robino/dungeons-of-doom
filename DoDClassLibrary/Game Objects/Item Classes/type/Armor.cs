﻿namespace DoDClassLibrary.Game_Objects.Item_Classes.type
{
    internal class Armor : Equipment
    {
        public Armor(string name, string icon, int defense = 0, int damage = 0) : base(name, icon, defense, damage)
        {
        }
    }
}