﻿namespace DoDClassLibrary.Game_Objects.Character_Classes
{
    public struct Stats
    {

        public int Agility;
        public int Luck;
        public int Strength;

        public Stats(int agility, int luck, int strength)
        {
            Agility = agility;
            Luck = luck;
            Strength = strength;

        }

        public Stats Add(Stats stats)
        {
            return new Stats(
                Agility + stats.Agility,
                Luck + stats.Luck,
                Strength + stats.Strength);
        }

        public Stats Add(int agility, int luck, int strength) => new Stats(
            Agility + agility,
            Luck + luck,
            Strength + strength);
    }
}