﻿using System;

namespace DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes
{
    internal class Enemy : Character
    {
        private static readonly Random Random = new Random();

        protected Enemy(string name, string icon) : base(name, icon)

        {
            DisplayPriority = 1;
            Level = 1;
            BaseStats = new Stats(
                Random.Next(10, 13),
                Random.Next(10, 13),
                Random.Next(10, 13));
            UppdateStats();
            Ammount++;
        }
        public static int Ammount { get; set; }

        public override string ToString()
        {
            return $"{Name} lvl: {Level} Hp: {HealthPoints} Dmg:{DamageMin} - {DamageMax}";
        }

        public override string LevelUp()
        {
            Level++;
            BaseStats = BaseStats.Add(5, 5, 5);
            UppdateStats();
            return $"The enemies have grown stronger!";
        }
    }
}