﻿namespace DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes.Type
{
    internal class Ogre : Enemy
    {
        private readonly string _name;

        public Ogre(string name, string icon) : base(name, icon)
        {
            _name = name;
        }

        protected override void Attack(Character opponent, OnAttack onAttack)
        {
            if (opponent.Level >= Level*2)
            {
                Dead = true;
                onAttack($"{Name} ran away in fear");
            }
            else
            {
                base.Attack(opponent, onAttack);
            }
        }
    }
}