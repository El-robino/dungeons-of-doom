﻿namespace DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes.Type
{
    internal class Orc : Enemy
    {
        private readonly string _name;

        public Orc(string name, string icon)
            : base(name, icon)
        {
            _name = name;
        }
    }
}