﻿namespace DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes.Type
{
    internal class Demon : Enemy
    {
        public Demon(string name, string icon)
            : base(name, icon)
        {
        }
    }
}