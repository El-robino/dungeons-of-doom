﻿namespace DoDClassLibrary.Game_Objects.Character_Classes.Enemy_Classes.Type
{
    internal class Troll : Enemy
    {
        public Troll(string name, string icon)
            : base(name, icon)
        {
        }
    }
}