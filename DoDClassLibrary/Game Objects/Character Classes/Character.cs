﻿using System;
using DoDClassLibrary.Utilities;
using DoDClassLibrary.World_Classes;

namespace DoDClassLibrary.Game_Objects.Character_Classes
{
    public abstract class Character : Pawn
    {
        public delegate void OnAttack(string message);

        private readonly Random _random = new Random();

        protected Character(string name, string icon) : base(name, icon)
        {
            Name = name;
        }

        public Square CurrentSquare { get; set; }

        public Stats BaseStats { get; set; }
        protected int HealthPoints { get; set; }
        public bool Dead { get; protected set; }
        protected int DamageMin { get; set; }
        protected int DamageMax { get; set; }
        protected int CritMultiplier { private get; set; }
        public int Level { get; protected set; }
        public abstract string LevelUp();

        protected virtual void UppdateStats()
        {
            HealthPoints = BaseStats.Strength * 8;
            DamageMin = BaseStats.Agility / 5 + BaseStats.Strength / 5;
            DamageMax = BaseStats.Agility / 3 + BaseStats.Strength / 3;
        }

        protected virtual Chance DodgeChance()
        {
            return new Chance(BaseStats.Agility / 5);
        }

        protected virtual Chance CritChance()
        {
            return new Chance(BaseStats.Luck / 2);
        }

        protected virtual void Attack(Character opponent, OnAttack onAttack)
        {
            var damage = _random.Next(DamageMin, DamageMax);
            if (opponent.DodgeChance().Evaluate())
                onAttack($"{opponent.Name} Dodged your attack!");
            else
            {
                if (CritChance().Evaluate())
                    damage *= CritMultiplier;

                opponent.HealthPoints -= damage;
                if (opponent.HealthPoints < 0)
                {
                    opponent.Dead = true;
                    onAttack($"{Name} killed {opponent.Name}  with {opponent.HealthPoints * -1} damage overkill!");
                }
                else
                {
                    onAttack($"{Name} attacked {opponent.Name} for {damage} damage! {opponent.HealthPoints} hp left");
                }
            }
        }

        public void Fight(Character opponent, OnAttack onAttack)
        {
            Attack(opponent, onAttack);
            if (!opponent.Dead)
            {
                opponent.Attack(this, onAttack);
            }
        }
    }
}