﻿using System;
using System.Collections.Generic;
using DoDClassLibrary.Game_Objects.Item_Classes;
using DoDClassLibrary.Game_Objects.Item_Classes.type;
using DoDClassLibrary.Utilities;

namespace DoDClassLibrary.Game_Objects.Character_Classes
{
    public class Player : Character
    {
        public Player(string name, string icon) : base(name, icon)
        {
            DisplayPriority = 2;
            Icon = icon;
            Experiance = 0;
            Level = 1;
            Name = name;
            var attribute = new Random();
            BaseStats = new Stats(
                attribute.Next(10, 13),
                attribute.Next(10, 13),
                attribute.Next(10, 13)
                );
            Inventory = new List<Item>();
            _equipment = new List<Equipment>()
            {
                new Armor("starter Armor", "s", damage:0, defense:5 ),
                new Weapon("Starter Mace", "m", damage:5)
            };
            UppdateStats();

        }
        private Stats EffectiveStats { get; set; }
        public List<Item> Inventory { get; }
        private readonly List<Equipment> _equipment;
        private int Experiance { get; }
        protected override Chance DodgeChance()
        {
            return new Chance(EffectiveStats.Agility / 5);
        }
        protected override Chance CritChance()
        {
            return new Chance(EffectiveStats.Luck / 2);
        }
        public override string LevelUp()
        {
            Level++;
            UppdateStats();
            return $"You Leveled up to level {Level}!";
        }

        protected sealed override void UppdateStats()
        {
            var damage = 0;
            EffectiveStats = BaseStats;
            foreach (var equipment in _equipment)
            {
                EffectiveStats = EffectiveStats.Add(equipment.Stats);
                damage += equipment.Damage;
            }
            DamageMin = damage / 4 + EffectiveStats.Strength / 4 + EffectiveStats.Agility / 4;
            DamageMax = damage / 2 + EffectiveStats.Strength / 4 + EffectiveStats.Agility / 4;
            CritMultiplier = 2;
            HealthPoints = EffectiveStats.Strength * 10;
        }
        public void Equip(Item item)
        {
            _equipment.Add((Equipment)item);
            UppdateStats();
            Inventory.Remove(item);
        }

        public override string ToString()
        {
            return $"Player: {Name} Level: {Level} Xp: {Experiance}/100\n" +
                   $"Stats: Agility {EffectiveStats.Agility}, Luck: {EffectiveStats.Luck}, Strength: {EffectiveStats.Strength}\n" +
                   $"Hp: {HealthPoints}\n" +
                   $"Damage: {DamageMin} - {DamageMax}\n";
        }
    }
}