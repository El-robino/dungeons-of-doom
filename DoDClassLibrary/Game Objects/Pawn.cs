﻿using System;

namespace DoDClassLibrary.Game_Objects
{
    public abstract class Pawn
    {
        protected Pawn(string name, string icon, int displayPriority = 0)
        {
            DisplayPriority = displayPriority;
            Icon = icon;
            Name = name;
            if (name.Length <= 2)
                throw new ArgumentException();
        }

        public int DisplayPriority { get; protected set; }
        protected string Name { get; set; }
        public string Icon { get; protected set; }
    }
}