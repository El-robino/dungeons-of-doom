﻿using System;

namespace DoDClassLibrary.Utilities
{
    public static class Generate
    {
        private static readonly Random MyRandom = new Random();

        public static string Name(string value)
        {
            var names = new[] {"Shitty", "Decent", "Best"};
            return $"{names[MyRandom.Next(names.Length)]} {value}";
        }
    }
}