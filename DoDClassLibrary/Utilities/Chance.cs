﻿using System;

namespace DoDClassLibrary.Utilities
{
    public struct Chance
    {
        private static readonly Random Random = new Random();
        private readonly int _value;
        private static readonly OpenSimplexNoise SimplexNoise = new OpenSimplexNoise();

        public Chance(int value)
        {
            if (value < 0 || value > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(value),
                    $"Chance cannot be outside inclusive range 0-100 . Value={value}");
            }
            _value = value;
        }

        public bool Evaluate()
        {
            return Random.Next(0, 100) < _value;
        }

        public static bool Evaluate(int value)
        {
            return Random.Next(0, 100) < value;
        }

        public bool Evaluate(int x, int y)
        {
            return SimplexNoise.Evaluate(x, y) > _value / 100d;
        }
    }
}