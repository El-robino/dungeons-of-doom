﻿using DoDClassLibrary;
using System;

namespace Dungeons_Of_Doom
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("       _.-;;-._");
            Console.WriteLine("'-..-'|   ||   |");
            Console.WriteLine("'-..-'|_.-;;-._|");
            Console.WriteLine("'-..-'|   ||   |");
            Console.WriteLine("'-..-'|_.-''-._|");
            var game = new Game(new GamePresenterWithColor());
            game.Start();
        }
    }
}