﻿
using DoDClassLibrary;
using DoDClassLibrary.Game_Objects.Character_Classes;
using DoDClassLibrary.Utilities;
using DoDClassLibrary.World_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeons_Of_Doom
{
    internal class GamePresenterWithColor : IGamePresenter, IMenu
    {
        public Queue<GameMessage> PrintQueue { get; }
        private List<ListItem<int>> LevelMenu { get; set; }
        public Control Control { get; private set; }
        private StatMenu ChoosenStat { get; set; }
        public GamePresenterWithColor()
        {
            Console.OutputEncoding = Encoding.Unicode;
            PrintQueue = new Queue<GameMessage>();
        }
        public void UserControls()
        {
            var consoleKey = Console.ReadKey().Key;

            Control = default(Control);
            switch (consoleKey)
            {
                case ConsoleKey.W:
                    Control = Control.MoveUp;
                    break;
                case ConsoleKey.S:
                    Control = Control.MoveDown;
                    break;
                case ConsoleKey.A:
                    Control = Control.MoveLeft;
                    break;
                case ConsoleKey.D:
                    Control = Control.MoveRight;
                    break;
                case ConsoleKey.E:
                    Control = Control.Use;
                    break;
                case ConsoleKey.R:
                    Control = Control.Inventory;
                    break;
                case ConsoleKey.Escape:
                    Control = Control.Escape;
                    break;
            }
        }
        public void PrintFromQueue()
        {
            while (PrintQueue.Any())
            {
                var dequeuedElement = PrintQueue.Dequeue();
                DisplayMessage(dequeuedElement.Message, dequeuedElement.Status);
            }
        }
        public StatMenu PickStat(Character character)
        {
            LevelMenu = new List<ListItem<int>>
            {
                new ListItem<int>("Agility", character.BaseStats.Agility),
                new ListItem<int>("Luck", character.BaseStats.Luck),
                new ListItem<int>("Strength", character.BaseStats.Strength)
            };
            // ReSharper disable once PossibleInvalidOperationException
            //Can only be null if parameter leaveoption is set to true.
            var choosenStat = ConsoleUtilities.Menu(LevelMenu, Printer, this, header: "LevelMenu").Value;
            switch (choosenStat)
            {
                case 0:
                    ChoosenStat = StatMenu.Agility;
                    break;
                case 1:
                    ChoosenStat = StatMenu.Luck;
                    break;
                case 2:
                    ChoosenStat = StatMenu.Strength;
                    break;
            }
            LevelMenu[choosenStat].Item++;
            return ChoosenStat;
        }
        public int? InventoryMenu(Player player)
        {
            var choosenItem = ConsoleUtilities.Menu(player.Inventory, Printer, this, "inventory", true);
            if (choosenItem == null) return null;
            PrintQueue.Enqueue(new GameMessage($"You Got the stats from {player.Inventory[choosenItem.Value]}", Status.Misc));
            return choosenItem;
        }

        public void NewFrame() => Console.Clear();

        public void ReadKeyInput() => Console.ReadKey(true);

        public string InputText() => Console.ReadLine();

        public void DisplayWorld(World world)
        {
            for (var y = 0; y < world.Height; y++)
            {
                for (var x = 0; x < world.Width; x++)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    var pawn = world.Squares[x, y].Pawns.OrderByDescending(o => o.DisplayPriority).FirstOrDefault();
                    Console.Write(pawn == null ? "[ ]" : $"[{pawn.Icon}]");
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
            }
        }
        public void DisplayPlayerInfo(Player player)
        {
            switch (player.Inventory.Count)
            {
                case 1:
                    DisplayMessage($"you got {player.Inventory.Count} item in your inventory. Press r to open inventory");
                    break;
                case 0:
                    DisplayMessage("you got no items in your inventory.");
                    break;
                default:
                    DisplayMessage($"you got {player.Inventory.Count} items in your inventory. Press r to open inventory");
                    break;
            }
            Console.WriteLine(player);
        }

        public void DisplayMessage(string header, Status status = Status.Default)
        {
            if (status == Status.Positive)
                Console.ForegroundColor = ConsoleColor.Green;
            if (status == Status.Negative)
                Console.ForegroundColor = ConsoleColor.Red;
            if (status == Status.Misc)
                Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine(header);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void Printer<T>(T type, bool selected) => Console.WriteLine((selected ? "→" : "") + $" {type}\n");
    }
}