﻿using System;
using System.Runtime.CompilerServices;

namespace Dungeons_Of_Doom.Utilities
{
    public class OpenSimplexNoise
    {
        private const double Stretch_2D = -0.211324865405187; //(1/Math.sqrt(2+1)-1)/2;
        private const double Squish_2D = 0.366025403784439; //(Math.sqrt(2+1)-1)/2;
        private const double Norm_2D = 2.0/47.0;

        private static readonly double[] Gradients2D =
        {
            5, 2, 2, 5,
            -5, 2, -2, 5,
            5, -2, 2, -5,
            -5, -2, -2, -5
        };

        private static readonly Contribution2[] Lookup2D;

        private readonly byte[] _perm;
        private readonly byte[] _perm2D;

        static OpenSimplexNoise()
        {
            var base2D = new[]
            {
                new[] {1, 1, 0, 1, 0, 1, 0, 0, 0},
                new[] {1, 1, 0, 1, 0, 1, 2, 1, 1}
            };
            var p2D = new[] {0, 0, 1, -1, 0, 0, -1, 1, 0, 2, 1, 1, 1, 2, 2, 0, 1, 2, 0, 2, 1, 0, 0, 0};
            var lookupPairs2D = new[] {0, 1, 1, 0, 4, 1, 17, 0, 20, 2, 21, 2, 22, 5, 23, 5, 26, 4, 39, 3, 42, 4, 43, 3};

            var contributions2D = new Contribution2[p2D.Length/4];
            for (var i = 0; i < p2D.Length; i += 4)
            {
                var baseSet = base2D[p2D[i]];
                Contribution2 previous = null, current = null;
                for (var k = 0; k < baseSet.Length; k += 3)
                {
                    current = new Contribution2(baseSet[k], baseSet[k + 1], baseSet[k + 2]);
                    if (previous == null)
                    {
                        contributions2D[i/4] = current;
                    }
                    else
                    {
                        previous.Next = current;
                    }
                    previous = current;
                }
                if (current != null) current.Next = new Contribution2(p2D[i + 1], p2D[i + 2], p2D[i + 3]);
            }

            Lookup2D = new Contribution2[64];
            for (var i = 0; i < lookupPairs2D.Length; i += 2)
            {
                Lookup2D[lookupPairs2D[i]] = contributions2D[lookupPairs2D[i + 1]];
            }
        }

        public OpenSimplexNoise()
        {
            var seed = DateTime.Now.Ticks;
            _perm = new byte[256];
            _perm2D = new byte[256];
            var source = new byte[256];
            for (var i = 0; i < 256; i++)
            {
                source[i] = (byte) i;
            }
            seed = seed*6364136223846793005L + 1442695040888963407L;
            seed = seed*6364136223846793005L + 1442695040888963407L;
            seed = seed*6364136223846793005L + 1442695040888963407L;
            for (var i = 255; i >= 0; i--)
            {
                seed = seed*6364136223846793005L + 1442695040888963407L;
                var r = (int) ((seed + 31)%(i + 1));
                if (r < 0)
                {
                    r += i + 1;
                }
                _perm[i] = source[r];
                _perm2D[i] = (byte) (_perm[i] & 0x0E);
                source[r] = source[i];
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int FastFloor(double x)
        {
            var xi = (int) x;
            return x < xi ? xi - 1 : xi;
        }

        public double Evaluate(double x, double y)
        {
            var stretchOffset = (x + y)*Stretch_2D;
            var xs = x + stretchOffset;
            var ys = y + stretchOffset;

            var xsb = FastFloor(xs);
            var ysb = FastFloor(ys);

            var squishOffset = (xsb + ysb)*Squish_2D;
            var dx0 = x - (xsb + squishOffset);
            var dy0 = y - (ysb + squishOffset);

            var xins = xs - xsb;
            var yins = ys - ysb;

            var inSum = xins + yins;

            var hash =
                (int) (xins - yins + 1) |
                (int) inSum << 1 |
                (int) (inSum + yins) << 2 |
                (int) (inSum + xins) << 4;

            var c = Lookup2D[hash];

            var value = 0.0;
            while (c != null)
            {
                var dx = dx0 + c.Dx;
                var dy = dy0 + c.Dy;
                var attn = 2 - dx*dx - dy*dy;
                if (attn > 0)
                {
                    var px = xsb + c.Xsb;
                    var py = ysb + c.Ysb;

                    var i = _perm2D[(_perm[px & 0xFF] + py) & 0xFF];
                    var valuePart = Gradients2D[i]*dx + Gradients2D[i + 1]*dy;

                    attn *= attn;
                    value += attn*attn*valuePart;
                }
                c = c.Next;
            }
            return value*Norm_2D;
        }

        private class Contribution2
        {
            public readonly double Dx;
            public readonly double Dy;
            public readonly int Xsb;
            public readonly int Ysb;
            public Contribution2 Next;

            public Contribution2(double multiplier, int xsb, int ysb)
            {
                Dx = -xsb - multiplier*Squish_2D;
                Dy = -ysb - multiplier*Squish_2D;
                Xsb = xsb;
                Ysb = ysb;
            }
        }
    }
}