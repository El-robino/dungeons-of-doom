﻿using System;
using System.Collections.Generic;

namespace Dungeons_Of_Doom.Utilities
{
    internal interface IMenu
    {
        void Printer<T>(T type, bool selected);
    }
    internal class ConsoleUtilities
    {
        private static int CurrentItem { get; set; }
        public static int? Menu<T>(List<T> listToPrint, Action<T, bool> printer, IGamePresenter presenter, string header, bool leaveOption = false)
        {
            if (CurrentItem >= listToPrint.Count)
                CurrentItem = 0;
            while (true)
            {
                Console.Clear();
                presenter.DisplayMessage(header);
                for (var i = 0; i < listToPrint.Count; i++)
                    printer(listToPrint[i], i == CurrentItem);
                presenter.UserControls(Console.ReadKey(true).Key);
                if (presenter.Control == Control.MoveDown && CurrentItem < listToPrint.Count - 1)
                    CurrentItem++;
                else if (presenter.Control == Control.MoveUp && CurrentItem > 0)
                    CurrentItem--;
                else if (presenter.Control == Control.Use && listToPrint.Count != 0)
                    return CurrentItem;
                else if (leaveOption && presenter.Control == Control.Escape)
                    return null;
            }
        }
    }
    public class ListItem<T>
    {
        public ListItem(string text, T type)
        {
            Text = text;
            Item = type;
        }
        private string Text { get; }
        public T Item { get; set; }

        public override string ToString()
        {
            return $"{Text}: {Item}";
        }
    }
}
